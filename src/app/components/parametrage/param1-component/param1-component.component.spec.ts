import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Param1ComponentComponent } from './param1-component.component';

describe('Param1ComponentComponent', () => {
  let component: Param1ComponentComponent;
  let fixture: ComponentFixture<Param1ComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Param1ComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Param1ComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
