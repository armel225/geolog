import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommuneComponentComponent } from './commune-component.component';

describe('CommuneComponentComponent', () => {
  let component: CommuneComponentComponent;
  let fixture: ComponentFixture<CommuneComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommuneComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommuneComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
