import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import {Subscription} from 'rxjs';

import { GeologService } from 'app/service/geolog.service';
import { CustomNotificationService } from 'app/service/custom-notification.service';
import { BsModalRef } from 'ngx-bootstrap';
import { BsModalService } from 'ngx-bootstrap/modal';
import { HelperService } from 'app/service/helper.service';


import * as moment from 'moment';
const Swal = require('sweetalert2')

@Component({
  selector: 'app-generaux',
  templateUrl: './generaux.component.html', 
  styleUrls: ['./generaux.component.css']
})
export class GenerauxComponent implements OnInit {

  datasSelect : Array<any> = [];
  requestSw : any;
  itemToSave : any;
  busyCommune : Subscription;
  response : any;
  communeModal : BsModalRef;
  requiredField : Array<any> = [];
  confirmParam : any;

  config = {
    animated: true,
    keyboard: false,
    backdrop: 'static',
    ignoreBackdropClick: true
  };

  constructor(private geologService : GeologService, private notificationService: CustomNotificationService,private modalService: BsModalService, private helper : HelperService ) {

    this.datasSelect = ["5", "10", "15", "20", "tous"];
    this.requestSw = {
      Index: 0,
      Size: 10,
      TakeAll: false,
      value: "10",
      SearchValue: ''
  };

  this.itemToSave = {};
  this.response = {
    Items: [],
    Count: 0
  };

  this.requiredField = [false];
  this.confirmParam = {
    title: 'Confirmation',
    text: "Voulez-vous poursuivre cette action ?",
    type: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Oui',
    cancelButtonText: 'Annuler',
    reverseButtons: true
  }

  }

  openCommune(template,itemToModified) {
    this.communeModal = this.modalService.show(template,Object.assign({},this.config, { class: 'gray modal-md' }));
    this.requiredField = [false];
    this.itemToSave = {};
  }
    

  saveCommune(item : any){
  
      //verifions les champs requis
      this.requiredField[0] = this.helper.isRequired(item.libelle);
      if (this.requiredField[0])
          return;
          

    Swal.fire(this.confirmParam).then((result) => {
      if (result.value) {
        item.createdBy = 1 //à remplacer
        let request = {
          ItemsToSave : [item]
         };

         let httpHeaders = new HttpHeaders()
         .set('Content-Type', 'application/json'); 
    
         let options = {
          headers: httpHeaders
         }; 

         this.busyCommune = this.geologService.post('SaveCommunes', request).subscribe(
          res =>{ 
            console.log("success " , res);
            this.response = res;
            
            if (!this.response.HasError) {
             this.notificationService.success();
            this.cancel();
            this.refreshTable();
            } else {
             this.notificationService.error(this.response.Message);
             
         }
         },
          err =>{
           console.log("erreur ", err); 
         }
        )

      } else if ( result.dismiss === Swal.DismissReason.cancel) {
          
        }
    })

  }

  cancel()
  {
    this.communeModal.hide();
    this.requiredField = [false];
    this.itemToSave = {};

  }

  refreshTable (){
    let request = {
      ItemToSearch: { libelle: this.requestSw.SearchValue }, //critère de recherche
      Index:this.requestSw.Index,
      Size: this.requestSw.value == "tous" ? 0 : this.requestSw.value,
      TakeAll: this.requestSw.value == "tous" ? true : this.requestSw.TakeAll
     };

     let httpHeaders = new HttpHeaders()
     .set('Content-Type', 'application/json'); 

     let options = {
      headers: httpHeaders
     }; 

     this.busyCommune = this.geologService.post('GetCommunesByCriteria', request).subscribe(
      res =>{ 
        console.log("success " , res);
        this.response = res;
        
        if (!this.response.HasError) {
         
        }
     },
      err =>{
       console.log("erreur ", err); 
     }
    )
  };

  saveItem(item)
  {
    console.log("le item a enregistrer", item);

    let dateFormat = moment(item.createAt).format("DD-MM-YYYY"); //Format de la date souhaitée

    console.log("le create at a enregistrer",dateFormat);

  }

  ngOnInit() {
    this.refreshTable();
    this.itemToSave.dateFormatJsonTest = "/Date(1530144000000+0530)/";
    var myDate = moment.utc('/Date(1530144000000+0530)/');
    console.log(moment(myDate).format('DD-MM-YYYY'));

    console.log("moment date",)
   
  }

}
