import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'pipeApp'
})
export class PipeAppPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return null;
  }

}

@Pipe({name: 'formatmillier'})
export class FormatMillerPipe implements PipeTransform {
    transform(input: any, count?: number) {
        if(!input) return;

        var nombre=input;
        nombre += '';
        var sep = ' ';
        var reg = /(\d+)(\d{3})/;

        while (reg.test(nombre)) {
            nombre = nombre.replace(reg, '$1' + sep + '$2');
        }

        return nombre;
    }
}


@Pipe({name: 'formatJsonDate'})
export class FormatJsonDatePipe implements PipeTransform {
    transform(input: any) {
        if(!input) return;

        var myDate = moment.utc(input);
        return moment(myDate).format('DD/MM/YYYY');

    }
}

