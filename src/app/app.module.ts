import { NgModule, ApplicationRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import {NgBusyModule} from 'ng-busy';

import { FormsModule } from '@angular/forms';
import { BusyModule } from "angular2-busy";
import Swal from 'sweetalert2'

/*
 * Platform and Environment providers/directives/pipes
 */
import { routing } from './app.routing'
// App is our top level component
import { AppComponent } from './app.component';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { AppState, InternalStateType } from './app.service';

// Core providers
import {CoreModule} from "./core/core.module";
import {SmartadminLayoutModule} from "./shared/layout/layout.module";

import {SmartadminModule} from "./shared/smartadmin.module";
import {SmartadminDatatableModule} from "./shared/ui/datatable/smartadmin-datatable.module";
import {SmartadminValidationModule} from "./shared/forms/validation/smartadmin-validation.module";

import {GeologService} from './service/geolog.service';
import { CustomNotificationService } from './service/custom-notification.service';

import { ModalModule } from 'ngx-bootstrap/modal';
import { Param1ComponentComponent } from './components/parametrage/param1-component/param1-component.component';
import { CommuneComponentComponent } from './components/parametrage/commune-component/commune-component.component';
import { CommuneComponent } from './components/parametrage/commune/commune.component';
import { GenerauxComponent } from './components/parametrage/generaux/generaux.component';
import { HelperService } from './service/helper.service';

import { BsDatepickerModule } from 'ngx-bootstrap';
import { PipeAppPipe,FormatJsonDatePipe,FormatMillerPipe } from './pipe/pipe-app.pipe';

// Application wide providers
const APP_PROVIDERS = [
  ...APP_RESOLVER_PROVIDERS,
  AppState,
  GeologService,
  CustomNotificationService,
  HelperService
];

type StoreType = {
  state: InternalStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void
};

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent, 
    Param1ComponentComponent,
    CommuneComponentComponent,
    CommuneComponent,
    GenerauxComponent,
    PipeAppPipe,
    FormatJsonDatePipe,
    FormatMillerPipe

  ],
  imports: [ // import Angular's modules
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    CommonModule,
    SmartadminModule,
    SmartadminDatatableModule,
    SmartadminValidationModule,
    BusyModule,
   // NgBusyModule,
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),

    CoreModule,
    SmartadminLayoutModule,

    routing
  ],
  exports: [
  ],
  providers: [ // expose our Services and Providers into Angular's dependency injection
    // ENV_PROVIDERS,
    APP_PROVIDERS
  ]
})
export class AppModule {
  constructor(public appRef: ApplicationRef, public appState: AppState) {}


}

