import { Injectable, OnInit, ViewChild } from '@angular/core';
import {ModalDirective} from "ngx-bootstrap";
import {NotificationService} from "../shared/utils/notification.service";
import {FadeInTop} from "../shared/animations/fade-in-top.decorator";

@Injectable()
export class CustomNotificationService {

  constructor(private notificationService: NotificationService) { }

  success() {

    this.notificationService.smallBox({
      title: "Succès",
      content: "Enregistrement effectué avec succès",
      color: "#46a046" ,
      timeout: 4000,
      icon: "fa fa-bell"
    });
  }

  error(message : string) {

    this.notificationService.smallBox({
      title: "Erreur",
      content: message ==null ? "Une erreur s'est produite. Veuillez réessayer" : message,
      color: "#d43739" ,
      timeout: 4000,
      icon: "fa fa-bell"
    });
  }
}
