// import { Injectable } from '@angular/core';
// import { RequestOptions, URLSearchParams} from '@angular/http';
// import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';


// @Injectable()
// export class GeologService {
//   baseUrl : any;

//   constructor(public http : HttpClient) { 
//     this.baseUrl = 'http://localhost:17001/AppServices';
//   }

//   post(endpoint:string, body:any, headers?: HttpHeaders){
//     return this.http.post(this.baseUrl+'/'+endpoint, body)
//   }
// }


import { Injectable } from '@angular/core';
import { RequestOptions, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import {HttpClient, HttpParams} from "@angular/common/http";


@Injectable()
export class GeologService {
    url: any;
    user :any;
    constructor(public http: HttpClient,) {
         this.url = "http://localhost:17001/AppServices";
    }


    get(endpoint: string, params?: any, options?: HttpParams) {

        return this.http.get(this.url + '/' + endpoint);
    }

    getOption(endpoint: string, params?: any, options?: any) {

        return this.http.get(this.url + '/' + endpoint,options);
    }

    postOption(endpoint: string, body: any, options: any) {
        return this.http.post(this.url + '/' + endpoint, body,options);
    }

    post(endpoint: string, body: any, options?: RequestOptions) {
        console.log(this.url + '/' + endpoint);
      
        return this.http.post(this.url + '/' + endpoint, body);
    }

    put(endpoint: string, body: any, options?: RequestOptions) {
        return this.http.put(this.url + '/' + endpoint, body);
    }

    delete(endpoint: string, options?: RequestOptions) {
        return this.http.delete(this.url + '/' + endpoint);
    }

    patch(endpoint: string, body: any, options?: RequestOptions) {
        return this.http.put(this.url + '/' + endpoint, body);
    }
}

